WITH

airlines AS (

    SELECT * FROM {{source('nyc-airport', 'nye_airlines')}}
    

)

SELECT 


    string_field_0 AS carrier
    , string_field_1 AS name


FROM airlines

