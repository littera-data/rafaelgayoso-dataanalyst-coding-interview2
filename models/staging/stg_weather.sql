WITH

weather AS (

    SELECT * FROM {{source('nyc-airport', 'nye_weather')}}


)
SELECT 

    CAST(CONCAT(year,'-', month, '-', day) AS DATE) AS date 
    , DATETIME(year, month, day,  hour, 00, 00) AS weather_time
    , CAST(CASE WHEN temp = 'NA' THEN NULL ELSE temp END AS FLOAT64) AS temp
    , CAST(CASE WHEN dewp = 'NA' THEN NULL ELSE dewp END AS FLOAT64) AS dewp
    , CAST(CASE WHEN humid = 'NA' THEN NULL ELSE humid END AS FLOAT64) AS humid
    , CAST(CASE WHEN wind_speed = 'NA' THEN NULL ELSE wind_speed END AS FLOAT64) AS wind_speed    
    , visib 
    , precip
    , CAST(CASE WHEN wind_gust = 'NA' THEN NULL ELSE wind_gust END AS FLOAT64) AS wind_gust
    , CAST(CASE WHEN pressure = 'NA' THEN NULL ELSE pressure END AS FLOAT64) AS pressure
    , CAST(CASE WHEN wind_dir = 'NA' THEN NULL ELSE wind_dir END AS FLOAT64) AS wind_dir
    , * EXCEPT(wind_gust, pressure, temp, dewp, humid, wind_dir, wind_speed, precip, visib)


FROM weather

WHERE TRUE




