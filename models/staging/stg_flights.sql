
WITH

flights AS (

    SELECT * FROM {{source('nyc-airport', 'nye_flights')}}


)
SELECT 

    CAST(CONCAT(year,'-', month, '-', day) AS DATE) AS date 
    -- , DATETIME(year,'-', month, '-', day, 'T', hour,':', minute) AS departure_time
    , DATETIME(year, month, day,  hour, 00, 00) AS departure_hour
    , DATETIME(year, month, day,  hour, minute, 00) AS departure_time
    , *


FROM flights



