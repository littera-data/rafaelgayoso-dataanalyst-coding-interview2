WITH main AS (
SELECT 

    date
    , COUNT(DISTINCT dest) AS destinations

FROM {{ref("stg_flights")}}

WHERE TRUE
-- and origin = 'EWR'

GROUP BY 1

ORDER BY date DESC

)

SELECT 

    -- date 
     ROUND(SUM(destinations) / COUNT(DISTINCT date)) AS avg_dest
    -- , AVG(destinations) AS avg_dest
    

FROM main

-- GROUP BY 1,2 

-- ORDER BY 1