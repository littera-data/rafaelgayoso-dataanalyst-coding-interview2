WITH flights AS (

    SELECT 

        *

    FROM {{ref("stg_flights")}}
) 
, airline AS (

    SELECT

    *

    FROM {{ref("stg_airlines")}} a

    WHERE TRUE
    AND a.name != 'name'
)
, planes AS (

    SELECT

    *

    FROM {{ref("stg_planes")}} p

    WHERE TRUE
    
)


SELECT 

    a.name
    -- , f.tailnum
    , COUNT(f.flight) AS flights 
    , RANK() OVER(ORDER BY COUNT(f.flight) DESC) AS flights_rank

FROM airline a 
LEFT JOIN flights f ON f.carrier = a.carrier
LEFT JOIN planes p ON p.tailnum = f.tailnum


GROUP BY 1

ORDER BY 2 DESC