WITH flights AS (

    SELECT 

        departure_hour
        , dep_delay 
        , flight 
        , origin
        , dest 

    FROM {{ref("stg_flights")}}
) 
, weather AS (

    SELECT

    *

    FROM {{ref("stg_weather")}} w

)

SELECT

       CONCAT(ROUND(CORR(f.dep_delay, w.temp) * 100, 2), '%') AS temperature_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.dewp) * 100, 2), '%') AS dewpoint_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.humid) * 100, 2), '%') AS humidity_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.wind_speed) * 100, 2), '%') AS wind_speed_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.wind_gust) * 100, 2), '%') AS wind_gust_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.precip) * 100, 2), '%') AS precipitation_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.pressure) * 100, 2), '%') AS pressure_correlation
     , CONCAT(ROUND(CORR(f.dep_delay, w.visib) * 100, 2), '%') AS visibility_correlation
     


    -- , CORR(f.dep_delay, w.dewp) AS dewp_corr
    -- , CORR(f.dep_delay, w.humid) AS humid_corr
    -- -- , CORR(f.dep_delay, w.wind_dir) AS wind_dir_corr
    -- , CORR(f.dep_delay, w.wind_speed) AS wind_speed_corr
    -- , CORR(f.dep_delay, w.wind_gust) AS wind_gust_corr
    -- , CORR(f.dep_delay, w.precip) AS precip_corr
    -- , CORR(f.dep_delay, w.pressure) AS pressure_corr
    -- , CORR(f.dep_delay, w.visib) AS visib_corr
    -- f.*
    -- , w.*

    -- COUNT(*)


FROM flights f
INNER JOIN weather w ON w.weather_time = f.departure_hour AND f.origin = w.origin

WHERE TRUE 


-- FROM flights
