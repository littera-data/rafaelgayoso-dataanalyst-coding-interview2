-- MOST COMMON PLANE MANUFACTURER

WITH flights AS (

    SELECT 

        *

    FROM {{ref("stg_flights")}}

    WHERE TRUE
    -- AND origin = 'EWR'
) 
, airline AS (

    SELECT

    *

    FROM {{ref("stg_airlines")}} a

    WHERE TRUE
    AND a.name != 'name'
)
, planes AS (

    SELECT

    *

    FROM {{ref("stg_planes")}} p

    WHERE TRUE
    
)


SELECT 

    manufacturer
    -- , f.tailnum
    , COUNT(f.flight) AS flights 
    , RANK() OVER(ORDER BY COUNT(f.flight) DESC) AS flights_rank


FROM flights f --ON f.carrier = a.carrier
LEFT JOIN planes p ON p.tailnum = f.tailnum


GROUP BY 1

ORDER BY 2 DESC
-- SELECT * FROM {{ref("stg_planes")}} 