WITH flights AS (

    SELECT 

        *

    FROM {{ref("stg_flights")}}
) 
, airline AS (

    SELECT

    *

    FROM {{ref("stg_airlines")}} a

    WHERE TRUE
    AND a.name != 'name'
)
, planes AS (

    SELECT

    *

    FROM {{ref("stg_planes")}} p

    WHERE TRUE
    
)

, last AS (

SELECT

    a.name
    -- , f.tailnum
    , COUNT(f.flight) AS flights 
    , SUM(p.seats) AS avaiable_seats

FROM airline a 
LEFT JOIN flights f ON f.carrier = a.carrier
LEFT JOIN planes p ON p.tailnum = f.tailnum


GROUP BY 1

ORDER BY 2 DESC

)

SELECT 

    name 
    , flights
    , avaiable_seats
    -- , ROW_NUMBER() OVER(PARTITION BY name ORDER BY avaiable_seats) AS rank
    , RANK() OVER(ORDER BY avaiable_seats DESC) AS seats_rank

FROM last

ORDER BY 3 DESC