WITH main AS (

SELECT 

    date 
    , FORMAT_TIMESTAMP('%A', date) AS day_of_week
    , COUNT(DISTINCT dest) AS destinations

FROM {{ref("stg_flights")}}

WHERE TRUE
-- and origin = 'EWR'

GROUP BY 1

ORDER BY date DESC

)

SELECT 

    day_of_week
    , COUNT(day_of_week) AS days
    , SUM(destinations) AS dest_sum
    , SUM(destinations) / COUNT(day_of_week) AS avg_dest_dow

FROM main

GROUP BY 1